package com.example.deploy_test.controller;

import com.example.deploy_test.database.User;
import com.example.deploy_test.database.UserDTO;
import com.example.deploy_test.repo.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/test/v1")
public class TestController {
    @Autowired
    UserRepo userRepo;

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public ResponseEntity<?> hello(@RequestBody UserDTO userDTO){
        User user = User.builder()
                .username(userDTO.getUsername())
                .name(userDTO.getName())
                .description(userDTO.getDescription())
                .build();
        user = userRepo.save(user);

        return ResponseEntity.ok(UserDTO.builder()
                .id(user.getId())
                .name(user.getName())
                .username(user.getUsername())
                .description(user.getDescription())
                .build());
    }

    @RequestMapping(value = "/user/{id}", method = RequestMethod.GET)
    public ResponseEntity<?> hello(@PathVariable long id){
        if(id == 0 || !userRepo.existsById(id)){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }
        User user = userRepo.findById(id)
                .get();

        return ResponseEntity.ok(UserDTO.builder()
                .id(user.getId())
                .name(user.getName())
                .username(user.getUsername())
                .description(user.getDescription())
                .build());
    }

    @RequestMapping(value = "/getAll", method = RequestMethod.GET)
    public ResponseEntity<?> hello(){
        return ResponseEntity.ok(userRepo.findAll());
    }
}
