FROM openjdk:8
MAINTAINER AnhDZ <anhh34711@gmail.com>
EXPOSE 8090
ADD ./target/deploy_test-0.0.1-SNAPSHOT.jar deploy_test-0.0.1-SNAPSHOT.jar
CMD ["java", "-jar", "/deploy_test-0.0.1-SNAPSHOT.jar"]

